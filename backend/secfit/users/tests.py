from django.test import TestCase, Client
from rest_framework.test import RequestsClient
from django.contrib.auth import get_user_model
from .serializers import UserSerializer


class UserTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.data = {'username': 'fred',
                     'password': 'secret',
                     'password1': 'secret',
                     'email': 'test@test.test',
                     'phone_number': '12345678',
                     'country': 'Norway',
                     'city': 'Oslo',
                     'street_address': 'street1',
                     'age': 20,
                     'expirience': 3,
                     'favorite_dicipline': 'Bench press',
                     'bio': 'I love working out'}

    def test_user_object_serialized(self):
        response = UserSerializer.create(self, self.data)
        user = get_user_model().objects.get(username="fred")
        self.assertEqual(response.username, "fred")
        self.assertEqual(user.city, "Oslo")

    def test_user_object_view(self):
        response = self.client.post('/api/users/', {'username': 'fred2',
                                                    'password': 'secret',
                                                    'password1': 'secret',
                                                    'email': 'test@test.test',
                                                    'phone_number': '12345678',
                                                    'country': 'Norway',
                                                    'city': 'Oslo',
                                                    'street_address': 'street1',
                                                    'age': 20,
                                                    'expirience': 3,
                                                    'favorite_dicipline': 'Bench press',
                                                    'bio': 'I love working out'})

        self.assertEqual(response.status_code, 201)
