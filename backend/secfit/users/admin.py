from django.contrib.auth import get_user_model
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Offer, AthleteFile
from .forms import CustomUserChangeForm, CustomUserCreationForm

# Register your models here.


class CustomUserAdmin(UserAdmin):
    """Allows users to have a custom
    user page in the Django admin view
    """
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = get_user_model()
    add_fieldsets = UserAdmin.add_fieldsets + ((None, {"fields": ("coach",)}),)


admin.site.register(get_user_model(), CustomUserAdmin)
admin.site.register(Offer)
admin.site.register(AthleteFile)
