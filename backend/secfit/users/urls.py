"""Contains paths in the api for the users application.
"""
from django.urls import path
from users import views

urlpatterns = [
    # Adding all the user paths in the api
    path(
        "api/users/",
        views.UserList.as_view(),
        name="user-list"
    ),
    path(
        "api/users/<int:pk>/",
        views.UserDetail.as_view(),
        name="user-detail"
    ),
    path(
        "api/profiles/<int:pk>/",
        views.ProfileUpdate.as_view(),
        name="profile-update"
    ),
    path(
        "api/users/<str:username>/",
        views.UserDetail.as_view(),
        name="user-detail"
    ),
    path(
        "api/offers/",
        views.OfferList.as_view(),
        name="offer-list"
    ),
    path(
        "api/offers/<int:pk>/",
        views.OfferDetail.as_view(),
        name="offer-detail"
    ),
    path(
        "api/athlete-files/",
        views.AthleteFileList.as_view(),
        name="athlete-file-list"
    ),
    path(
        "api/athlete-files/<int:pk>/",
        views.AthleteFileDetail.as_view(),
        name="athletefile-detail",
    ),
]
