from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from .models import Group, Membership, Content, Comment, Like


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a group instance.

    Serialized fields: id, owner, name, description.

    Attributes:
        owner:  The owner is the creator of the group and is read only
    """
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = Group
        fields = ["id", "owner", "name", "description"]


class MembershipSerializer(serializers.ModelSerializer):
    """Serializer for a membership instance

    Serialized fields: id, member, group
    """
    class Meta:
        model = Membership
        fields = ["id", "member", "group"]


class ContentSerializer(serializers.ModelSerializer):
    """Serializer for an Exercise. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, name, description, duration, calories, muscle group, unit, instances

    Attributes:
        instances:  Associated exercise instances with this Exercise type. Hyperlinks.
    """

    class Meta:
        model = Content
        fields = ["id", "group", "title", "description", "image"]
        creator = serializers.ReadOnlyField(source='creator.id')
        image = serializers.Field(required=False)


class CommentSerializer(serializers.ModelSerializer):
    """Serializer for a membership instance

    Serialized fields: id, member, group
    """
    class Meta:
        model = Comment
        fields = ["id", "content", "message"]
        owner = serializers.ReadOnlyField(source="owner.username")


class LikeSerializer(serializers.ModelSerializer):
    """Serializer for a membership instance

    Serialized fields: id, member, group
    """
    class Meta:
        model = Like
        fields = ["id", "content"]
        owner = serializers.ReadOnlyField(source="owner.username")
