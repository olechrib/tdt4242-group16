from django.db import models

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.


class Group(models.Model):
    """Django model for an instance of a workout group.

    Attributes:
        owner:      The owner of the group who is also the creator
        name:       The name of the group.
        description:a description of the group, its purpose and fitness information
    """
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="groupOwner"
    )
    name = models.TextField()
    description = models.TextField()


class Membership(models.Model):
    """Django model for members of groups

    Attributes:
        member:     Specifies the user who is a member.
        group:       Specifies the group that the user is a member of.
    """
    member = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="member"
    )
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, related_name="memberOf"
    )

    class Meta:
        # restriction so that no user is a member two times in a group.
        unique_together = ('member', 'group')


class Content(models.Model):
    creator = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="creator"
    )
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, related_name="ownedByGroup"
    )
    title = models.TextField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='media', blank=True)


class Comment(models.Model):
    """Django model for a comment left on a workout.

    Attributes:
        owner:       Who posted the comment
        workout:     The workout this comment was left on.
        content:     The content of the comment.
        timestamp:   When the comment was created.
    """
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="commentOwner"
    )
    content = models.ForeignKey(
        Content, on_delete=models.CASCADE, related_name="relatedContent"
    )
    message = models.TextField()


class Like(models.Model):
    """Django model for a comment left on a workout.

    Attributes:
        owner:       Who posted the comment
        workout:     The workout this comment was left on.
        content:     The content of the comment.
        timestamp:   When the comment was created.
    """
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="likeOwner"
    )
    content = models.ForeignKey(
        Content, on_delete=models.CASCADE, related_name="likedContent"
    )

    class Meta:
        unique_together = ('owner', 'content')
