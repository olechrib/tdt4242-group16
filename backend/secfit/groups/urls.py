from django.urls import path
from .views import GroupView, MembershipView, getGroup, ContentList, PostContent, PostComment, CommentList, PostLike


urlpatterns = [
    path("api/groups/", GroupView.as_view(), name="group-view"),
    path("api/members/", MembershipView.as_view(), name="membership-view"),
    path("api/groups/<int:pk>/", getGroup.as_view(), name="updateGroup-view"),
    path("api/content/", PostContent.as_view(), name="postContent-view"),
    path("api/content/<int:pk>/", ContentList.as_view(), name="contentList-view"),
    path("api/comment/", PostComment.as_view(), name="postComment-view"),
    path("api/comment/<int:pk>/", CommentList.as_view(), name="CommentList-view"),
    path("api/like/", PostLike.as_view(), name="postLike-view"),
]
