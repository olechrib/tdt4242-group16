from django.contrib import admin

# Register your models here.
from .models import Group, Membership, Content, Comment, Like

admin.site.register(Group)
admin.site.register(Membership)
admin.site.register(Content)
admin.site.register(Comment)
admin.site.register(Like)
