from django.shortcuts import render
from rest_framework import generics, mixins
from .models import Group, Membership, Content, Comment, Like
from rest_framework import permissions
from .serializers import GroupSerializer, MembershipSerializer, ContentSerializer, CommentSerializer, LikeSerializer
from rest_framework.filters import OrderingFilter


class GroupView(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    generics.GenericAPIView,
):
    """
    View for getting a list of all groups, and for creating a new group.

    HTTP methods: GET, POST
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [OrderingFilter]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class getGroup(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    generics.GenericAPIView,
):
    """
    View for getting a specified group, and for updating a specified group

    HTTP methods: GET, PUT
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class MembershipView(
    mixins.CreateModelMixin,
    generics.GenericAPIView,
):
    """
    View adding new members to a group

    HTTP methods: POST
    """
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class ContentList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of content, or
    a list of content.

    HTTP methods: GET, POST
    """

    queryset = Content.objects.all()
    serializer_class = ContentSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        qs = Content.objects.all()
        pk = self.kwargs['pk']
        qs = Content.objects.filter(group=pk)
        return qs


class PostContent(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of content, or
    a list of content.

    HTTP methods: GET, POST
    """

    queryset = Content.objects.all()
    serializer_class = ContentSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class PostComment(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of content, or
    a list of content.

    HTTP methods: GET, POST
    """

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class CommentList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of content, or
    a list of content.

    HTTP methods: GET, POST
    """

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        qs = Comment.objects.all()
        pk = self.kwargs['pk']
        qs = Comment.objects.filter(content=pk)
        return qs


class PostLike(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of content, or
    a list of content.

    HTTP methods: GET, POST
    """

    queryset = Like.objects.all()
    serializer_class = LikeSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
