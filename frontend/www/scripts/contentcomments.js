/**
 * Sendes a get request to the API to retrieve a list of all the groups
 * If the response is ok then each group instance is placed in html code
 * to make a list of all the groups.
 */
 async function fetchComments(id) {
    let response = await sendRequest("GET", `${HOST}/api/comment/${id}`);

    if (response.ok) {
        let data = await response.json();

        let comments = data.results;
        let container = document.getElementById('div-comment');
        let commentTemplate = document.querySelector("#template-comment");
        comments.forEach(comment => {
            const commentAnchor = commentTemplate.content.firstElementChild.cloneNode(true);
            //contentAnchor.href = `group.html?id=${content.id}`;

            const h5 = commentAnchor.querySelector("h5");
            h5.textContent = comment.owner;

            const p = commentAnchor.querySelector("p");
            p.textContent = comment.message;   

            container.appendChild(commentAnchor);
        });
    }

    return response;
}

async function addComment() {
    let form = document.querySelector("#form-comment");
    let formData = new FormData(form);
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('id')) {
        const contentId = urlParams.get('id');
        let body = {"content": contentId,
                    "message": formData.get("comment"),
                    };

        let response = await sendRequest("POST", `${HOST}/api/comment/`, body);

        if (response.ok) {
            window.location.replace(`contentcomments.html?id=${contentId}`);
        } else {
            let data = await response.json();
            let alert = createAlert("Could not create new group!", data);
            document.body.prepend(alert);
        }
    }

}

async function like() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('id')) {
        const contentId = urlParams.get('id');
        let body = {"content": contentId};

        let response = await sendRequest("POST", `${HOST}/api/like/`, body);

        if (response.ok) {
            window.location.replace(`contentcomments.html?id=${contentId}`);
        } else {
            let data = await response.json();
            let alert = createAlert("Could not create new group!", data);
            document.body.prepend(alert);
        }
    }

}

/**
 * When a user enters groups.html then the groups are fetched.
 */
window.addEventListener("DOMContentLoaded", async () => {
    let addButton = document.querySelector("#btn-add-comment");
    addButton.addEventListener("click", async () => await addComment());
    let likeButton = document.querySelector("#btn-like");
    likeButton.addEventListener("click", async () => await like());
    

    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has('id')) {
        const contentId = urlParams.get('id');
        await fetchComments(contentId);

        if (!response.ok) {
            let data = await response.json();
            let alert = createAlert("Could not retrieve groups!", data);
            document.body.prepend(alert);
        }
    }    

});
