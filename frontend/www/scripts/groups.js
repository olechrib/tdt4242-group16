/**
 * Sendes a get request to the API to retrieve a list of all the groups
 * If the response is ok then each group instance is placed in html code
 * to make a list of all the groups.
 */
async function fetchGroups(request) {
    let response = await sendRequest("GET", `${HOST}/api/groups/`);

    if (response.ok) {
        let data = await response.json();

        let groups = data.results;
        let container = document.getElementById('div-content');
        let groupTemplate = document.querySelector("#template-group");
        groups.forEach(group => {
            const groupAnchor = groupTemplate.content.firstElementChild.cloneNode(true);
            groupAnchor.href = `groupContent.html?id=${group.id}`;

            const h5 = groupAnchor.querySelector("h5");
            h5.textContent = group.name;

            const p = groupAnchor.querySelector("p");
            p.textContent = group.description;   

            container.appendChild(groupAnchor);
        });
    }

    return response;
}

function createGroup() {
    window.location.replace("group.html");
}

/**
 * When a user enters groups.html then the groups are fetched.
 */
window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-group");
    createButton.addEventListener("click", createGroup);

    let response = await fetchGroups();
    
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve groups!", data);
        document.body.prepend(alert);
    }
});
