/**
 * Sendes a get request to the API to retrieve a list of all the groups
 * If the response is ok then each group instance is placed in html code
 * to make a list of all the groups.
 */
 async function fetchContent(id) {
    let response = await sendRequest("GET", `${HOST}/api/content/${id}`);

    if (response.ok) {
        let data = await response.json();

        let contents = data.results;
        let container = document.getElementById('div-content');
        let contentTemplate = document.querySelector("#template-content");
        contents.forEach(content => {
            const contentAnchor = contentTemplate.content.firstElementChild.cloneNode(true);
            contentAnchor.href = `contentcomments.html?id=${content.id}`;

            const h5 = contentAnchor.querySelector("h5");
            h5.textContent = content.title;

            const p = contentAnchor.querySelector("p");
            p.textContent = content.description;   

            container.appendChild(contentAnchor);
        });
    }

    return response;
}

function addContent() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('id')) {
        const groupId = urlParams.get('id');
        window.location.replace(`addcontent.html?id=${groupId}`);
    }

}

function editGroup() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('id')) {
        const groupId = urlParams.get('id');
        window.location.replace(`group.html?id=${groupId}`);
    }
}

/**
 * When a user enters groups.html then the groups are fetched.
 */
window.addEventListener("DOMContentLoaded", async () => {
    let addButton = document.querySelector("#btn-add-content");
    addButton.addEventListener("click", addContent);
    let editButton = document.querySelector("#btn-edit-group");
    editButton.addEventListener("click", editGroup);
    

    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has('id')) {
        const groupId = urlParams.get('id');
        await fetchContent(groupId);

        if (!response.ok) {
            let data = await response.json();
            let alert = createAlert("Could not retrieve groups!", data);
            document.body.prepend(alert);
        }
    } 

    

});
