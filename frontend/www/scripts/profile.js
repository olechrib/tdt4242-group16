let cancelButton;
let okButton;
let editButton;
let oldFormData;


function handleCancelButtonDuringCreate() {
    window.location.replace("profile.html");
}

/**
 * If user presses "cancel" during editing the profile the
 * form fields become read only and the form data is deleted.
 * And the buttons change.
 */
function handleCancelButtonDuringEdit() {
    setReadOnly(true, "#form-profile");
    okButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-profile");
    if (oldFormData.has("age")) form.name.value = oldFormData.get("age");
    if (oldFormData.has("expirience")) form.description.value = oldFormData.get("expirience");
    if (oldFormData.has("dicipline")) form.description.value = oldFormData.get("dicipline");
    if (oldFormData.has("bio")) form.description.value = oldFormData.get("bio");

    oldFormData.delete("age");
    oldFormData.delete("expirience");
    oldFormData.delete("dicipline");
    oldFormData.delete("bio");
}

/**
 * If the user clicks on the edit button the form fields can be edited.
 * And the form is updated with the profile data the user wants to change.
 */
function handleEditProfileButtonClick() {
    setReadOnly(false, "#form-profile");

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-profile");
    oldFormData = new FormData(form);
}

/**
 * sends an API request to retrieve the profile information of the user that is currently active
 */

async function getCurrentProfile() {
    let res = await sendRequest("GET", `${HOST}/api/users/?user=current`);
    if (!res.ok) {
      console.log("COULD NOT RETRIEVE CURRENTLY LOGGED IN USER");
  } else {
      let data = await res.json();
      userID = data.results[0].id;
      userName = data.results[0].username
  }
    document.getElementById("title").innerHTML = userName + "'s Fitness Profile";
    let response = await sendRequest("GET", `${HOST}/api/users/${userID}/`);
    if (!response.ok) {
      console.log("COULD NOT RETRIEVE CURRENTLY LOGGED IN USER");
  } else {
      let profileData = await response.json();
      let form = document.querySelector("#form-profile");
        let formData = new FormData(form);
        for (let key of formData.keys()) {
            let selector
            selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = profileData[key];
            input.value = newVal;
        }
    }
}

/**
 * Sends a PUT request to the API to update the active users profile information.
 */
async function updateProfile() {
    let res = await sendRequest("GET", `${HOST}/api/users/?user=current`);
    if (!res.ok) {
      console.log("COULD NOT RETRIEVE CURRENTLY LOGGED IN USER");
  } else {
      let data = await res.json();
      userID = data.results[0].id;
  }
    let form = document.querySelector("#form-profile");
    let formData = new FormData(form);

    let body = {"age": formData.get("age"),
                "expirience": formData.get("expirience"),
                "favorite_dicipline": formData.get("favorite_dicipline"),
                "bio": formData.get("bio"),
                };
    let response = await sendRequest("PUT", `${HOST}/api/profiles/${userID}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update profile`, data);
        document.body.prepend(alert);
    } else {
        setReadOnly(true, "#form-profile");
        okButton.className += " hide";
        cancelButton.className += " hide";
        editButton.className = editButton.className.replace(" hide", "");

        cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

        oldFormData.delete("age");
        oldFormData.delete("expirience");
        oldFormData.delete("dicipline");
        oldFormData.delete("bio");
    }
}

/**
 * When a user enters the profile.html his/her profile information is retrieved
 */

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-profile");
    okButton = document.querySelector("#btn-ok-profile");
    editButton = document.querySelector("#btn-edit-profile");
    oldFormData = null;
    await getCurrentProfile();
    editButton.addEventListener("click", handleEditProfileButtonClick);
    okButton.addEventListener("click", async () => await updateProfile());
});