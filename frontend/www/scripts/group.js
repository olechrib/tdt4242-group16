let cancelButton;
let okButton;
let editButton;
let oldFormData;
let inviteButton;
let inviteForm;


function handleCancelButtonDuringCreate() {
    window.location.replace("groups.html");
}

/**
 * If user presses "cancel" during editing a group the 
 * form fields become read only and the form data is deleted.
 * And the buttons change.
 */
function handleCancelButtonDuringEdit() {
    setReadOnly(true, "#form-group");
    okButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-group");
    if (oldFormData.has("name")) form.name.value = oldFormData.get("name");
    if (oldFormData.has("description")) form.description.value = oldFormData.get("description");
    
    oldFormData.delete("name");
    oldFormData.delete("description");
}

/**
 * This api request is fired when the user clicks the button to create a group
 * It sends a POST request with the form data, and relocates to groups.html or
 * sends an error message in respons. 
 */
async function createGroup() {
    let form = document.querySelector("#form-group");
    let formData = new FormData(form);
    let body = {"name": formData.get("name"), 
                "description": formData.get("description"),
                };

    let response = await sendRequest("POST", `${HOST}/api/groups/`, body);

    if (response.ok) {
        window.location.replace("groups.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new group!", data);
        document.body.prepend(alert);
    }
}

/**
 * If the user clicks on the edit button the form fields can be edited.
 * And the form is updated with the data of the group that the user wants to edit.
 */
function handleEditGroupButtonClick() {
    setReadOnly(false, "#form-group");

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-group");
    oldFormData = new FormData(form);
}

/**
 * sends an API request to retrieve the group information of the group that 
 * the user wants to edit.
 * @param {integer} id of the group that the user wants to see/edit
 */
async function retrieveGroup(id) {
    let response = await sendRequest("GET", `${HOST}/api/groups/${id}/`);

    console.log(response.ok)

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve group data!", data);
        document.body.prepend(alert);
    } else {
        let groupData = await response.json();
        let form = document.querySelector("#form-group");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector
            selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = groupData[key];
            input.value = newVal;
        }
    }
}

/**
 * Sends a PUT request to the API to update a group's information.
 * @param {integer} id of the group to be updated
 */
async function updateGroup(id) {
    let form = document.querySelector("#form-group");
    let formData = new FormData(form);

    let body = {"name": formData.get("name"), 
                "description": formData.get("description"),
                };
    let response = await sendRequest("PUT", `${HOST}/api/groups/${id}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update group ${id}`, data);
        document.body.prepend(alert);
    } else {
        setReadOnly(true, "#form-group");
        okButton.className += " hide";
        cancelButton.className += " hide";
        editButton.className = editButton.className.replace(" hide", "");
    
        cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
        
        oldFormData.delete("name");
        oldFormData.delete("description");
    }
}

/**
 * sends a POST request to the API to add a new member to the group.
 * @param {*} id of the group that is getting the new member
 */

async function inviteMember(id) {
    let inviteForm = document.querySelector("#form-invite");
    let formData = new FormData(inviteForm);

    let body = {"member": formData.get("userId"), 
                "group": id,
                };
    let response = await sendRequest("POST", `${HOST}/api/members/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not invite member ${id}`, data);
        document.body.prepend(alert);
    } else {
        let data = await response.json();
        let alert = createAlert(`Member ${id} invited`);
        document.body.prepend(alert);
    }
}

/**
 * When a user enters the group.html this decides whether it
 * is entered in view/edit mode or in create mode. If the html contains
 * a url parameter with an id it is entered in view/edit mode.
 */

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-group");
    okButton = document.querySelector("#btn-ok-group");
    editButton = document.querySelector("#btn-edit-group");
    inviteButton = document.querySelector("#btn-invite-group");
    inviteForm = document.querySelector("#form-invite");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const groupId = urlParams.get('id');
        await retrieveGroup(groupId);

        editButton.addEventListener("click", handleEditGroupButtonClick);
        okButton.addEventListener("click", (async (id) => await updateGroup(id)).bind(undefined, groupId));
        inviteButton.addEventListener("click", (async (id) => await inviteMember(id)).bind(undefined, groupId));
    } 
    //create
    else {
        setReadOnly(false, "#form-group");

        inviteForm.className += " hide";
        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => await createGroup());
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
    }
});