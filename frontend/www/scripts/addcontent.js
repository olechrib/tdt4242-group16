let cancelButton;
let okButton;

function handleCancelButtonDuringCreate() {
    window.location.replace("groups.html");
}

/**
 * This api request is fired when the user clicks the button to add content to a group
 * It sends a POST request with the form data, and relocates to groupsContent page or
 * sends an error message in respons. 
 */
async function addContent() {
    let form = document.querySelector("#form-content");
    let formData = new FormData(form);
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('id')) {
        const groupId = urlParams.get('id');
        const input = document.querySelector('input[type="file"]');
        let body = {"group": groupId,
                    "title": formData.get("title"),
                    "description": formData.get("description"),
                    };

        let response = await sendRequest("POST", `${HOST}/api/content/`, body);

        if (response.ok) {
            window.location.replace(`groupContent.html?id=${groupId}`);
        } else {
            let data = await response.json();
            let alert = createAlert("Could not create new group!", data);
            document.body.prepend(alert);
        }
    }
}

/**
 * When a user enters the group.html this decides whether it
 * is entered in view/edit mode or in create mode. If the html contains
 * a url parameter with an id it is entered in view/edit mode.
 */

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-addcontent");
    okButton = document.querySelector("#btn-ok-addcontent");
    okButton.addEventListener("click", async () => await addContent());
    cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
});