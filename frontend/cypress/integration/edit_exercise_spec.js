/**
 * Helper function to login as a user and receive tokens and
 * necesarry authentication.
 */
Cypress.Commands.add('login', (username, password) => {
  cy.visit('/login.html')
  cy.get('input[name="username"]').type(username)
  cy.get('input[name="password"]').type(password)
  cy.intercept({method: 'POST', url:'/api/token/'}).as('postToken')
  cy.get('#btn-login').click()
  cy.wait('@postToken').its('response.statusCode').should('eq', 200)
})

Cypress.Commands.add('edit', (code) => {
  cy.intercept({method: 'PUT', url:'/api/exercises/*'}).as('putExercise')
  cy.get('#btn-ok-exercise').click()
  cy.wait('@putExercise').its('response.statusCode').should('eq', code)
})

describe('Testing boundary value on view/edit exercise page', () => {
  beforeEach(() => {
    cy.login('user', 'secure')
    // Wait for the view/edit page to load
    cy.visit('/exercise.html?id=1')
    cy.intercept('/api/exercises/*').as('getExercises')
    cy.wait('@getExercises').its('response.statusCode').should('eq', 200)
    // enable editing
    cy.get('#btn-edit-exercise').click()
  })

 

  /**
   * The backend model states that there is an upper boundary of 50 characters.
   * And that it cannot be empty. meaning boundary of 1-50 character length.
   * This is not enforces in the HTML form, but only backend. 
   */
  it('Testing upper boundaries for input length for Unit', () => {

    // testing for 51
    cy.get('input[name="unit"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651')
    cy.edit(400)
    cy.get('@putExercise').then((interception) => {
      assert.equal(interception.response.body.unit, 'Ensure this field has no more than 50 characters.')
    })
    cy.contains('Ensure this field has no more than 50 characters.')
    // testing for 50
    cy.get('input[name="unit"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY65')
    cy.edit(200)
    cy.get('input[name="unit"]').should('have.value', 'Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY65')

  })

  it('Testing lower boundaries for input length for Unit', () => {
    // testing for 0
    cy.get('input[name="unit"]').clear()
    cy.edit(400)
    cy.get('@putExercise').then((interception) => {
      assert.equal(interception.response.body.unit, 'This field may not be blank.')
    })
    cy.contains('This field may not be blank.')
    // testing for 1
    cy.get('input[name="unit"]').clear().type('Y')
    cy.edit(200)
    cy.get('input[name="unit"]').should('have.value', 'Y')
  })

   /**
   * The lower boundaries on duration and calories are not enforced and
   * therefore the tests will fail.
   * It is possible to test for upper boundaries as well, but the tests will
   * fail because they are not enforced.
   */

  it('Testing lower boundaries for duration', () => {
    // Testing frontend HTML form validation lower boundaries
    cy.get('input[name="duration"]').clear().type('-1').then(($input) => {
      expect($input[0].validationMessage).to.equal('[Error message]')
    })
    cy.get('input[name="duration"]').clear().type('0').then(($input) => {
      expect($input[0].validationMessage).to.equal('')
    })

    // Testing boundaries backend
    // testing for -1
    cy.get('input[name="duration"]').clear().type('-1')
    cy.edit(400)
    cy.get('@putExercise').then((interception) => {
      assert.equal(interception.response.body.duration, '[Error message]')
    })
    cy.contains('[Error message]')
    // testing for 0
    cy.get('input[name="duration"]').clear().type('0')
    cy.edit(200)
    cy.get('input[name="duration"]').should('have.value', '0')

  })
  it('Testing lower boundaries for calories', () => {
    // Testing frontend HTML form validation lower boundaries
    cy.get('input[name="calories"]').clear().type('-1').then(($input) => {
      expect($input[0].validationMessage).to.equal('[Error message]')
    })
    cy.get('input[name="calories"]').clear().type('0').then(($input) => {
      expect($input[0].validationMessage).to.equal('')
    })
    // Testing boundaries backend
    // testing for -1
    cy.get('input[name="calories"]').clear().type('-1')
    cy.edit(400)
    cy.get('@putExercise').then((interception) => {
      assert.equal(interception.response.body.calories, '[Error message]')
    })
    cy.contains('[Error message]')
    // testing for 0
    cy.get('input[name="calories"]').clear().type('0')
    cy.edit(200)
    cy.get('input[name="calories"]').should('have.value', '0')
  })

})