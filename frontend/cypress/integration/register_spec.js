
/**
 * Helper function to help determine whether registration was successful or not.
 * It intercepts the POST requesst to the /users/ API and checks if the
 * request was successful and thus the response should have a status code of 201
 * orif it failed and thus should have a status code of 400
 * @param {integer} code is a response status code, either 201 or 400
 */
Cypress.Commands.add('register', (code) => {
  cy.intercept({method: 'POST', url:'/api/users/'}).as('postUser')
  cy.get('#btn-create-account').click()
  cy.wait('@postUser').its('response.statusCode').should('eq', code)
  if (code == 201) {
    cy.visit('/logout.html')
  }
})

Cypress.Commands.add('visitRegister', () => {
  cy.visit('/register.html')
  cy.inputValidName()
  cy.get('input[name="email"]').type('test@test.test')
  cy.get('input[name="password"]').type('secure')
  cy.get('input[name="password1"]').type('secure')
})

/**
 * Helper function to generate a unique name to avoid "username already exists"
 */
Cypress.Commands.add('inputValidName', () => {
  const uuid = () => Cypress._.random(0, 1e6)
  const id = uuid()
  const name = `username${id}`
  cy.get('input[name="username"]').clear().type(name)
})

describe('Testing boundary value on register page', () => {
  beforeEach(() => {
    cy.visit('/register.html')

    // adding valid data to the required input fields
    cy.inputValidName()
    cy.get('input[name="password"]').type('secure')
    cy.get('input[name="password1"]').type('secure')
  
  })
  it('Testing boundaries for username', () => {
    // input length lower bound
    cy.get('input[name="username"]').clear()
    // register and check that response status code was 400
    cy.register(400)
    // testing that the response body gives the right error response for username
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.username, 'This field may not be blank.')
    })
    // checking that the error response is visible on the page.
    cy.contains('This field may not be blank.')
    cy.visitRegister()
    // Testing with 1 character
    cy.get('input[name="username"]').clear().type('s')
    cy.register(201)
    cy.visitRegister()

    // input length upper bound
    // testing with 151 characters
    cy.get('input[name="username"]').clear().type('OsLqErW9DfJcme7NJapjQ81SUcjCJvoCtbrMp1sQCGypyGNf4XM97JjGlg4I1VlDR5JFGeUYMTClcoSCKeAxyC21GKsCgpFHl2UulwtHudoC760umqQjGjK9OEciTuS97eg3yuWjjJx3Mtg6GyQK8V1')
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.username, 'Ensure this field has no more than 150 characters.')
    })
    cy.contains('Ensure this field has no more than 150 characters.')
    cy.visitRegister()

    // Testing with 150 character
    cy.get('input[name="username"]').clear().type('OsLqErW9DfJcme7NJapjQ81SUcjCJvoCtbrMp1sQCGypyGNf4XM97JjGlg4I1VlDR5JFGeUYMTClcoSCKeAxyC21GKsCgpFHl2UulwtHudoC760umqQjGjK9OEciTuS97eg3yuWjjJx3Mtg6GyQK8V')
    cy.register(201)
  })

  it('Testing boundaries for passwords', () => {
    // Input length lower bound testing with 1 character should work
    cy.get('input[name="password"]').clear().type('s')
    cy.get('input[name="password1"]').clear().type('s')
    cy.register(201)
    cy.visitRegister()

    // Input length lower boundary for passwords with 0 characters
    cy.get('input[name="password"]').clear()
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.password, 'This field may not be blank.')
    })
    cy.contains('This field may not be blank.')
    cy.visitRegister()

    cy.get('input[name="password1"]').clear()
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.password1, 'This field may not be blank.')
    })
    cy.contains('This field may not be blank.')
    cy.visitRegister()
  })

  it('Testing input length boundaries for phone_number, country, city and street', () => {
    // Testing with upper bound 50 characters. Should work
    cy.get('input[name="phone_number"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY65')
    cy.register(201)
    cy.visitRegister()

    cy.get('input[name="country"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY65')
    cy.register(201)
    cy.visitRegister()

    cy.get('input[name="city"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY65')
    cy.register(201)
    cy.visitRegister()

    cy.get('input[name="street_address"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY65')
    cy.register(201)
    cy.visitRegister()

    // testing with upper bound 51 characters. should raise error.
    cy.get('input[name="phone_number"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651')
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.phone_number, 'Ensure this field has no more than 50 characters.')
    })
    cy.contains('Ensure this field has no more than 50 characters.')
    cy.visitRegister()

    cy.get('input[name="country"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651')
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.country, 'Ensure this field has no more than 50 characters.')
    })
    cy.contains('Ensure this field has no more than 50 characters.')
    cy.visitRegister()

    cy.get('input[name="city"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651')
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.city, 'Ensure this field has no more than 50 characters.')
    })
    cy.contains('Ensure this field has no more than 50 characters.')
    cy.visitRegister()

    cy.get('input[name="street_address"]').clear().type('Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651')
    cy.register(400)
    cy.get('@postUser').then((interception) => {
      assert.equal(interception.response.body.street_address, 'Ensure this field has no more than 50 characters.')
    })
    cy.contains('Ensure this field has no more than 50 characters.')
    cy.visitRegister()

    // these fields have no lower boundary in backend.
  })

  it('Testing that email field is the right format', () => {
    // Invalid email input should raise validation error message
    cy.get('input[name="email"]').clear().type('not_an_email').then(($input) => {
      expect($input[0].validationMessage).to.not.equal('')
    })

    // Sending wrong input is not approved and should not redirect
    cy.get('#btn-create-account').click()
    cy.contains('Enter a valid email address.')
    cy.url().should('be.equal', 'http://localhost:9090/register.html')

    // checking valid email input
    cy.get('input[name="email"]').clear().type('an_email@test.test').then(($input) => {
      expect($input[0].validationMessage).to.equal('')
    })
  })
  
  it('Testing that all frontend HTML form fields have lower boundary length 1', () => {
    // Sending an empty form should give 8 invalid input validation errors.
    // These errors are not displayed on the page, but since the HTML form fields have
    // "reqired" attribute a validation message will be raised.
    cy.get('input[name="username"]').clear()
    cy.get('input[name="email"]').clear()
    cy.get('input[name="password"]').clear()
    cy.get('input[name="password1"]').clear()
    cy.get('input[name="phone_number"]').clear()
    cy.get('input[name="country"]').clear()
    cy.get('input[name="city"]').clear()
    cy.get('input[name="street_address"]').clear()

    cy.get('#btn-create-account').click()
    // Since all are required, then 8 fields should be invalid.
    cy.get('input:invalid').should('have.length', 8)
  })
})