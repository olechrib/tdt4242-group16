Cypress.Commands.add('login', (username, password) => {
  cy.visit('/login.html')
  cy.get('input[name="username"]').type(username)
  cy.get('input[name="password"]').type(password)
  cy.intercept({method: 'POST', url:'/api/token/'}).as('postToken')
  cy.get('#btn-login').click()
  cy.wait('@postToken').its('response.statusCode').should('eq', 200)
})

describe('Testing group functionality and fitness profile', () => {
  beforeEach(() => {
    cy.login('user', 'secure')
    // wait for the exercise to be rertrieved from backend.
    cy.visit('/groups.html')
    cy.intercept('/api/groups/').as('getGroups')
    cy.wait('@getGroups')
  })

  it('FR29: Testing creating new group', () => {
    cy.get('#btn-create-group').click()
    cy.url().should('contain', '/group.html')
    
    // typing data for new group
    cy.get('input[name="name"]').type('My Group')
    cy.get('textarea[name="description"]').type('This is my group')
    cy.intercept({method: 'POST', url:'/api/groups/'}).as('postGroup')
    cy.get('#btn-ok-group').click()
    // waiting for response from backend and checking if group was created.
    cy.wait('@postGroup').its('response.statusCode').should('eq', 201)

    // if response ok, then we should be redirected to the groups page.
    cy.url().should('contain', '/groups.html')
    cy.intercept('/api/groups/').as('getGroups')
    // intercepting the get request and checking that the new group is
    // among the elements we retrieve.
    cy.wait('@getGroups').then((interception) => {
      assert.equal(interception.response.body.name, 'My Group')
    })
    // checking that our group is rendered in the groups list.
    cy.contains('My Group')
    cy.contains('This is my group')
  })

  it('FR30: Testing editing group', () => {
    // Finding the group to edit and clicking on it
    cy.contains('My Group').click()
    cy.url().should('include', '/groupContent.html?id=')
    
    // clicking on the edit button and waiting until the data has loaded
    cy.get('#btn-edit-group').click()
    cy.url().should('include', '/group.html?id=')
    cy.intercept('/api/groups/*').as('getGroup')
    cy.wait('@getGroup')

    // updating the group info
    cy.get('#btn-edit-group').click()
    cy.get('input[name="name"]').clear().type('My Group (edited)')
    cy.get('textarea[name="description"]').clear().type('This is my group (edited)')
    // confirming the edit and checking that i is updated
    cy.intercept({method: 'PUT', url:'/api/groups/*'}).as('putGroup')
    cy.get('#btn-ok-group').click()
    cy.wait('@putGroup').its('response.statusCode').should('eq', 200)
    
    // Check that the edited group appears
    cy.visit('/groups.html')
    cy.contains('My Group (edited)')
    cy.contains('This is my group (edited)')
  })

  it('FR31: Testing inviting users', () => {
    // Finding the group to to invite users to
    cy.contains('My Group').click()
    cy.get('#btn-edit-group').click()
    
    // Inviting user with ID one and expecting response OK
    cy.get('input[name="userId"]').clear().type('1')
    cy.intercept({method: 'POST', url:'/api/members/'}).as('postMember')
    cy.get('#btn-invite-group').click()
    cy.wait('@postMember')

    // Inviting same meber again should result in error.
    cy.get('#btn-invite-group').click()
    cy.wait('@postMember').its('response.statusCode').should('eq', 400)
    cy.get('@postMember').then((interception) => {
      assert.equal(interception.response.body.non_field_errors, 'The fields member, group must make a unique set.')
    })
  })

  it('FR32: Testing adding content to group', () => {
    cy.contains('My Group (edited)').click()
    // adding input for the group content
    cy.get('#btn-add-content').click()
    cy.get('input[name="title"]').clear().type('My Content')
    cy.get('textarea[name="description"]').clear().type('This is content description')
    
    // Adding content and checking that the request is accepted.
    cy.intercept({method: 'POST', url:'/api/content/'}).as('postContent')
    cy.get('#btn-ok-addcontent').click()
    cy.wait('@postContent').its('response.statusCode').should('eq', 201)
    cy.url().should('contain', '/groupContent.html?id=')
    cy.intercept('/api/content/*').as('getContent')
    // intercepting the request and checking that it includes the new content
    cy.wait('@getContent').then((interception) => {
      assert.equal(interception.response.body.results[0].title, 'My Content')
    })
    // checking that the new content is displayed on the page
    cy.contains('My Content')
    cy.contains('This is content description')
  })

  it('FR33: Testing adding comments and liking group content', () => {
    // finding the right group and content to post a comment to.
    cy.contains('My Group (edited)').click()
    cy.contains('My Content').click()
    cy.url().should('include', '/contentcomments.html?id=')

    // typing and adding a comment
    cy.get('textarea[name="comment"]').type('This is a comment')
    cy.intercept({method: 'POST', url:'/api/comment/'}).as('postComment')
    cy.get('#btn-add-comment').click()
    cy.wait('@postComment').its('response.statusCode').should('eq', 201)

    cy.intercept('/api/comment/*').as('getComments')
    // check if comment was retrieved from backend
    cy.wait('@getComments').then((interception) => {
      assert.equal(interception.response.body.results[0].message, 'This is a comment')
    })
    // check if comment was added
    cy.contains('This is a comment')

    // liking content
    cy.intercept('/api/like/').as('like')
    cy.get('#btn-like').click()
    cy.wait('@like')

    // liking again should not work
    cy.intercept('/api/like/').as('like')
    cy.get('#btn-like').click()
    cy.wait('@like').its('response.statusCode').should('eq', 500)
  })

  it('FR34: Testing adding info for fitness profile', () => {
    // wait for fitness profile data to load from backend
    cy.visit('/profile.html')
    cy.intercept('/api/users/*').as('getFitnessProfile')
    cy.wait('@getFitnessProfile').then((interception) => {
      assert.isNotNull(interception.response.body, 'API call has data')
    })
    cy.url().should('contain', '/profile.html')
    cy.wait('@getFitnessProfile')
    cy.get('#btn-edit-profile').click()
    
    // typing data for updating profile
    cy.get('input[name="age"]').clear().type('26')
    cy.get('input[name="expirience"]').clear().type('5')
    cy.get('textarea[name="favorite_dicipline"]').clear().type('Crossfit')
    cy.get('textarea[name="bio"]').clear().type('This is my bio')
    cy.get('#btn-ok-profile').click()
    
    // checking that the data was updated frontend.
    cy.get('input[name="age"]').should('have.value', '26')
    cy.get('input[name="expirience"]').should('have.value', '5')
    cy.get('textarea[name="favorite_dicipline"]').should('have.value', 'Crossfit')
    cy.get('textarea[name="bio"]').should('have.value', 'This is my bio')
  })
})