/**
 * Helper function for logging in.
 */
Cypress.Commands.add('login', (username, password) => {
    cy.visit('/login.html')
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('#btn-login').click()
    cy.url().should('contain', '/workouts.html')
  })

Cypress.Commands.add('logout', () => {
  cy.visit('/logout.html')
  cy.url().should('contain', '/index.html')
})

describe('Testing group functionality and fitness profile', () => {
  beforeEach(() => {
    // Two user profiles with below info are needed to run tests.
    // athlete needs to have user "coach" as coach.
    cy.login('athlete', 'secure')
    // cy.login('coach', 'secure')

    // Adding test data
    // three workouts with same owner but different visibility
    cy.get('#btn-create-workout').click()
    cy.get('input[name="name"]').type("My public workout")
    cy.get('input[name="date"]').type('2017-06-01T08:30')
    cy.get('textarea[name="notes"]').type('This is a note')
    cy.get('#inputVisibility').select('Public')
    cy.get('#btn-ok-workout').click()

    cy.get('#btn-create-workout').click()
    cy.get('input[name="name"]').type("My private workout")
    cy.get('input[name="date"]').type('2017-06-01T08:30')
    cy.get('textarea[name="notes"]').type('This is a note')
    cy.get('#inputVisibility').select('Private')
    cy.get('#btn-ok-workout').click()

    cy.get('#btn-create-workout').click()
    cy.get('input[name="name"]').type("My coach workout")
    cy.get('input[name="date"]').type('2017-06-01T08:30')
    cy.get('textarea[name="notes"]').type('This is a note')
    cy.get('#inputVisibility').select('Coach')
    cy.get('#btn-ok-workout').click()


  })
  /**
   * This test fails because the visibility does not workas intended.
   */
  it('Testing visibility as owner, coach, user, and guest', () => {
    // testing visibility as owner
    cy.contains('My public workout')
    cy.contains('My coach workout')
    cy.contains('My private workout')

    // testing visibility as coach
    cy.logout()
    cy.login('coach', 'secure')
    cy.contains('My public workout')
    cy.contains('My coach workout')

    // testing visibility as another athelete
    cy.logout()
    cy.login('athlete2', 'secure')
    cy.contains('My public workout')

    // testing visibility as visitor
    cy.logout()
    cy.visit('workouts.html')
    cy.contains('My public workout')
  })
})