/**
 * Lists with [name of input field, correct input, wrong input]
 * All inputs are testet against eachother at least once.
 */
let username = ['username', 'secure', ' ']
let email = ['email', 'test@test.test', 'test']
let password = ['password', 'secure', ' ']
let password1 = ['password1', 'secure', ' ']
let phone = ['phone_number', '12345678', 'Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651']
let country = ['country', 'Norway', 'Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651']
let city = ['city', 'Oslo', 'Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651']
let address = ['street_address', 'MyAddress', 'Ye9cxDpRYmw1kaQoFxTtMs40jUgo0SiS63PyqFiZzjzerqcY651']

/**
 * function to help determine whether registration was successful or not.
 * It intercepts the POST requesst to the /users/ API and checks if the
 * request was successful and thus the response should have a status code of 201
 * orif it failed and thus should have a status code of 400
 * @param {integer} code is a response status code, either 201 or 400
 * @param {list} inputList is one of the lists above
 * @param {integer} input is the index of the input being used in the inputList
 */
Cypress.Commands.add('register', (code, inputList, input) => {
  // cypress types the input into the chosen input field
  if (inputList && input) {
    cy.get(`input[name=${inputList[0]}]`).clear().type(inputList[input])
  }
  cy.intercept({method: 'POST', url:'/api/users/'}).as('postUser')
  cy.get('#btn-create-account').click()
  // intercepts the request and checks if the status code is correct
  cy.wait('@postUser').its('response.statusCode').should('eq', code)
  if (code == 201) {
    cy.visit('/logout.html')
  }
  cy.visit('/register.html')
  cy.get('input[name="username"]').type('username')
  cy.get('input[name="email"]').type('test@test.test')
  cy.get('input[name="password"]').type('secure')
  cy.get('input[name="password1"]').type('secure')
})

/**
 * Helper function to generate a unique name to avoid "username already exists"
 */
Cypress.Commands.add('inputValidName', () => {
  const uuid = () => Cypress._.random(0, 1e6)
  const id = uuid()
  const name = `username${id}`
  cy.get('input[name="username"]').clear().type(name)
})

describe('Testing two way domain for register', () => {
  beforeEach(() => {
    cy.visit('/register.html')

    cy.get('input[name="username"]').type('username')
    cy.get('input[name="email"]').type('test@test.test')
    cy.get('input[name="password"]').type('secure')
    cy.get('input[name="password1"]').type('secure')
  
  })

  it('Testing with correct inputs, and correct other inputs', () => {
    /**
     * To avoid "user already exist" error inputValidUsername is used
     * to generate a unique and valid username.
     * This tests all valid inputs against all other valid inputs
     */
    cy.inputValidName()
    cy.get('input[name="email"]').clear().type(email[1])
    cy.register(201)

    cy.inputValidName()
    cy.get('input[name="password"]').clear().type(password[1])
    cy.register(201)

    cy.inputValidName()
    cy.get('input[name="password1"]').clear().type(password1[1])
    cy.register(201)

    cy.inputValidName()
    cy.get('input[name="phone_number"]').clear().type(phone[1])
    cy.register(201)

    cy.inputValidName()
    cy.get('input[name="country"]').clear().type(country[1])
    cy.register(201)

    cy.inputValidName()
    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(201)

    cy.inputValidName()
    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(201)
  })
  
  it('Testing with correct username, and wrong other inputs', () => {
    cy.get('input[name="username"]').clear().type('username')

    cy.get('input[name="email"]').clear().type(email[2])
    cy.register(400)

    cy.get('input[name="password"]').clear().type(password[2])
    cy.register(400)

    cy.get('input[name="password1"]').clear().type(password1[2])
    cy.register(400)

    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400)
  })

  it('Testing with wrong username, and correct other input', () => {
    cy.get('input[name="email"]').clear().type(email[1])
    cy.register(400, username, 2)

    cy.get('input[name="password"]').clear().type(password[1])
    cy.register(400, username, 2)

    cy.get('input[name="password1"]').clear().type(password1[1])
    cy.register(400, username, 2)

    cy.get('input[name="phone_number"]').clear().type(phone[1])
    cy.register(400, username, 2)

    cy.get('input[name="country"]').clear().type(country[1])
    cy.register(400, username, 2)

    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(400, username, 2)

    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, username, 2)
  })

  it('Testing with wrong username, and wrong other inputs', () => {
    cy.get('input[name="email"]').clear().type(email[2])
    cy.register(400, username, 2)

    cy.get('input[name="password"]').clear().type(password[2])
    cy.register(400, username, 2)

    cy.get('input[name="password1"]').clear().type(password1[2])
    cy.register(400, username, 2)

    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, username, 2)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, username, 2)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, username, 2)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, username, 2)
  })

  /**
   * Testing with email against other inputs except username which
   * has already been tested.
   */
  
  it('Testing with correct email, and wrong other inputs', () => {

    cy.get('input[name="password"]').clear().type(password[2])
    cy.register(400, email, 1)

    cy.get('input[name="password1"]').clear().type(password1[2])
    cy.register(400, email, 1)

    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, email, 1)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, email, 1)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, email, 1)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, email, 1)
  })

  it('Testing with wrong email, and correct other input', () => {
    cy.get('input[name="password"]').clear().type(password[1])
    cy.register(400, email, 2)

    cy.get('input[name="password1"]').clear().type(password1[1])
    cy.register(400, email, 2)

    cy.get('input[name="phone_number"]').clear().type(phone[1])
    cy.register(400, email, 2)

    cy.get('input[name="country"]').clear().type(country[1])
    cy.register(400, email, 2)

    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(400, email, 2)

    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, email, 2)
  })

  it('Testing with wrong email, and wrong other inputs', () => {
    cy.get('input[name="password"]').clear().type(password[2])
    cy.register(400, email, 2)

    cy.get('input[name="password1"]').clear().type(password1[2])
    cy.register(400, email, 2)

    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, email, 2)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, email, 2)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, email, 2)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, email, 2)
  })

  /**
   * Password
   */
  it('Testing with correct password, and wrong other inputs', () => {
    cy.get('input[name="password1"]').clear().type(password1[2])
    cy.register(400, password, 1)

    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, password, 1)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, password, 1)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, password, 1)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, password, 1)
  })

  it('Testing with wrong password, and correct other input', () => {

    cy.get('input[name="password1"]').clear().type(password1[1])
    cy.register(400, password, 2)

    cy.get('input[name="phone_number"]').clear().type(phone[1])
    cy.register(400, password, 2)

    cy.get('input[name="country"]').clear().type(country[1])
    cy.register(400, password, 2)

    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(400, password, 2)

    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, password, 2)
  })

  it('Testing with wrong password, and wrong other inputs', () => {
    cy.get('input[name="password1"]').clear().type(password1[2])
    cy.register(400, password, 2)

    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, password, 2)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, password, 2)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, password, 2)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, password, 2)
  })

  /**
   * Password1
   */
  it('Testing with correct password1, and wrong other inputs', () => {
    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, password1, 1)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, password1, 1)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, password1, 1)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, password1, 1)
  })

  it('Testing with wrong password1, and correct other input', () => {
    cy.get('input[name="phone_number"]').clear().type(phone[1])
    cy.register(400, password1, 2)

    cy.get('input[name="country"]').clear().type(country[1])
    cy.register(400, password1, 2)

    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(400, password1, 2)

    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, password1, 2)
  })

  it('Testing with wrong password1, and wrong other inputs', () => {
    cy.get('input[name="phone_number"]').clear().type(phone[2])
    cy.register(400, password1, 2)

    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, password1, 2)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, password1, 2)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, password1, 2)
  })

  /**
   * phone_number
   */
  it('Testing with correct phone_number, and wrong other inputs', () => {
    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, phone, 1)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, phone, 1)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, phone, 1)
  })

  it('Testing with wrong phone_number, and correct other input', () => {
    cy.get('input[name="country"]').clear().type(country[1])
    cy.register(400, phone, 2)

    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(400, phone, 2)

    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, phone, 2)
  })

  it('Testing with wrong phone_number, and wrong other inputs', () => {  
    cy.get('input[name="country"]').clear().type(country[2])
    cy.register(400, phone, 2)

    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, phone, 2)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, phone, 2)
  })

  /**
   * country
   */
  it('Testing with correct country, and wrong other inputs', () => {
    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, country, 1)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, country, 1)
  })

  it('Testing with wrong country, and correct other input', () => {
    cy.get('input[name="city"]').clear().type(city[1])
    cy.register(400, country, 2)

    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, country, 2)
  })

  it('Testing with wrong country, and wrong other inputs', () => {  
    cy.get('input[name="city"]').clear().type(city[2])
    cy.register(400, country, 2)

    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, country, 2)
  })
  
  /**
   * city
   */
  it('Testing with correct city, and wrong other inputs', () => {
    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, city, 1)
  })
  it('Testing with wrong city, and correct other input', () => {
    cy.get('input[name="street_address"]').clear().type(address[1])
    cy.register(400, city, 2)
  })
  it('Testing with wrong city, and wrong other inputs', () => {  
    cy.get('input[name="street_address"]').clear().type(address[2])
    cy.register(400, city, 2)
  })
})