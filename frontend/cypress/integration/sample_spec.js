// Arrange - setup initial app state
// - Visit a web page
// - query for an element
// Act - taken an action
// -interanct with that element
// Assert - make an assertion
// - make assertion about page content


describe('My First Test', () => {
  it('Finds an element', () => {
    cy.visit('/register.html')

    cy.get('input[name="username"]')
      .type('simen')
      .should('have.value', 'simen')
  })
})

  